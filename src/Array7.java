import java.util.Scanner;

public class Array7 {
    /*
     * จากโปรแกรมในข้อ 6 ให้นำเอาตัวเลขที่ได้มาหาค่าต่ำสุด
     * Please input arr[0]: 20
     * Please input arr[1]: 30
     * Please input arr[2]: 40
     * arr = 20 30 40
     * sum = 90
     * avg = 30.0000
     * min = 20
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[3];
        for (int i = 0; i < arr.length; i++) {
            System.out.println("Please input arr[" + i + "]");
            arr[i] = sc.nextInt();
        }
        System.out.println("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i] + " ");
        }

        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
        }
        System.out.println("sum = " + sum);
        
        double avg = sum / arr.length;
        System.out.println("avg = " + avg);
        
        int min = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if(min>arr[i]) {
                min = arr[i];
            }
        /*int index = 0;
        for (int i = 1; i < arr.length; i++) {
            if(arr[index]>arr[i]) {
                min = arr[i];
                } */
        }
        System.out.println("min = " + min);
    }
}
