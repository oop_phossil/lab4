import java.util.Scanner;

public class Array6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[3];
        for (int i = 0; i < arr.length; i++) {
            System.out.println("Please input arr[" + i + "]");
            arr[i] = sc.nextInt();
        }
        System.out.println("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i] + " ");
        }

        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
        }
        System.out.println("sum = " + sum);
        double avg = sum / arr.length;
        System.out.println("avg = " + avg);
    }
}