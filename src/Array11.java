import java.util.Scanner;

public class Array11 {
    /*
     * จงเขียนเกมส์เรียงตัวเลขโดยกำหนดให้มี array ขนาด 1 มิติขนาด 5 ตัว
     * โดยที่แต่ละตัวจะมีตัวเลขจำนวนเต็มอยู่ในช่วงระหว่าง 0-99 กำหนดไว้ ด้วยวิธีการ
     * random จากนั้นเกมส์จะให้ผู้ใช้บอกตัวเลข index ที่จะสลับกัน ถ้าตัวเลขภายใน
     * array เรียงจากน้อยไปมาก เกมส์นี้จะสิ้นสุด และบอกว่าผู้เล่นชนะเกมส์
     * ตัวอย่างโปรแกรมเป็นดังต่อไปนี้
     * Array
     * 3 5 2 1 4
     * Please input index: 1 2
     * 3 2 5 1 4
     * Please input index: 0 3
     * 1 2 5 3 4
     * Please input index: 2 3
     * 1 2 3 5 4
     * Please input index: 3 4
     * 1 2 3 4 5
     * You win!!!
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[5];
        int first, second;
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int)(Math.random()*100);
        }

        while (true) {

            for (int i = 0; i < arr.length; i++) {
                System.out.println(arr[i] + " ");
            }
            System.out.println();

            System.out.print("Please input index: ");
            first = sc.nextInt();
            second = sc.nextInt();
            System.out.println();

            // Swap
            int temp = arr[first];
            arr[first] = arr[second];
            arr[second] = temp;

            boolean isFinish = true;
            for (int i = 1; i < arr.length; i++) {
                if (arr[i-1] > arr[i]) {
                    isFinish = false;
                }
            }
            if(isFinish) {
                for (int i = 0; i < arr.length; i++) {
                    System.out.println(arr[i] + " ");
                }
                System.out.println();
                System.out.println("You Win!!!");
                break;
            }
        }
    }
}
