import java.util.Scanner;

public class MyApp {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        printWelcome();
        int num = 0;
        while (true) {
            inputChoice();
            num = sc.nextInt();
            if (num == 1 ) {
                printHelloWorldTime();
            }
            else if (num == 2) {
                addTwoNumber();
            }
            else if (num == 3) {
                break;
            }
            else {
                System.out.println("Error: Please input between 1-3");
            }
        }
        System.out.println("Bye!!");
    }
    static void printWelcome() {
        System.out.println("Welcome to my app!!!");
        printMenu();
    }
    static void printMenu() {
        System.out.println(
            "--Menu--\n" +
            "1. Print Hello World N times\n" +
            "2. Add 2 number\n" +
            "3. Exit\n"
        );
    }
    static void inputChoice() {
        System.out.println("Please input your choice(1-3): ");
    }
    static void printHelloWorldTime() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please input time: ");
        int time = sc.nextInt();
        if (time>0){
            for (int i = 0; i < time; i++) {
                System.out.println("Hello World!!!");
            }
        }
    }
    static void addTwoNumber() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please input first number: ");
        int num1 = sc.nextInt();
        System.out.println("Please input second number: ");
        int num2 = sc.nextInt();
        System.out.println("Result = " + (num1 + num2));
    }

}
