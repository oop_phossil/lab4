import java.util.Scanner;

public class Array10 {

    /*
     * จงเขียนโปรแกรมเพื่อหาตัวเลขที่มีทั้งหมดใน array ขนาด n ตัว (ไม่เกิน 100 ตัว)
     * ที่ไม่ซ้ำกับตัวอื่น กำหนดให้รับ input จากผู้ใช้ เข้า array และพิมพ์
     * ตัวเลขที่ไม่ซ้ำกันออกทางหน้าจอ
     * Please input number of element: 4
     * Element 0: 3
     * Element 1: 4
     * Element 2: 3
     * Element 3: 5
     * All number: 3 4 5
     */
    public static void main(String[] args) {
        int size = 0;
        int arr[];
        int uniSize = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Please input number of element: ");
        size = sc.nextInt();
        arr = new int[size];
        for (int i = 0; i < arr.length; i++) {
            System.out.println("Element " + i + ": ");
            int temp = sc.nextInt();
            int index = -1;
            for (int j = 0; j < uniSize; j++) {
                if (arr[j] == temp) {
                    index = j;
                }
            }
            if (index < 0) {
                arr[uniSize] = temp;
                uniSize++;
            }
        }
        System.out.println("arr = ");
        for (int i = 0; i < uniSize; i++) {
            System.out.println(arr[i] + " ");
        }
    }
}
