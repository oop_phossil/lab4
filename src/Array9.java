import java.util.Scanner;

public class Array9 {
/*จากโปรแกรมในข้อ 3 ให้นำเอาตัวเลขที่ได้มาหาค่าที่ต้องการหา หากหาไม่พบให้แสดงว่า not found แต่ถ้าพบให้แสดง index ที่พบตัวเลขนั้นครั้งแรก
Case 1
Please input arr[0]: 20
Please input arr[1]: 30
Please input arr[2]: 40
arr = 20 30 40
please input search value: 30
found at index: 1
Case 2
Please input arr[0]: 20
Please input arr[1]: 30
Please input arr[2]: 40
arr = 20 30 40
please input search value: 31
not found
*/ 
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[3];
        for(int i=0; i<arr.length;i++) {
            System.out.println("Please input arr[" + i + "]");
            arr[i] = sc.nextInt();
        }
        System.out.println("arr = ");
        for(int i =0; i<arr.length;i++ ) {
          System.out.println(arr[i] + " ");
        }
        
        System.out.println("please in put search value: ");
        int searchValue = sc.nextInt();
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == searchValue) {
              index = i;
              break;
            }
        }
        if(index>0) {
            System.out.println("found at index: " + index);
        } else {
            System.out.println("not found"); 
        }
    }
}
